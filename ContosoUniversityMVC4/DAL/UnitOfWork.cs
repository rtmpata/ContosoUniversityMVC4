﻿using ContosoUniversityMVC4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContosoUniversityMVC4.DAL
{
    public class UnitOfWork : IDisposable
    {
        private SchoolContext _context = new SchoolContext();
        private GenericRepository<Course> _courseRepository;
        private GenericRepository<Department> _departmentRepository;

        public GenericRepository<Department> DepartmentRepository
        {
            get
            {

                if (this._departmentRepository == null)
                {
                    this._departmentRepository = new GenericRepository<Department>(_context);
                }
                return _departmentRepository;
            }
        }

        public GenericRepository<Course> CourseRepository
        {
            get
            {
                if(this._courseRepository == null)
                {
                    this._courseRepository = new GenericRepository<Course>(_context);
                }
                return _courseRepository;
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }


        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}